//Опишите своими словами разницу между функциями setTimeout() и setInterval().

//setTimeout() вызывает функцию только один раз, а setInterval() вызывает функцию каждый раз с промежутком опреденого времени


// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?

//нет, потому что он выполнет сначала всё а потом возьмет то что написано в setTimeout()


// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

// чтобы закочить действие setInterval()


//В папке banners лежит HTML код и папка с картинками.
//При запуске программы на экране должна отображаться первая картинка.
// Через 3 секунды вместо нее должна быть показана вторая картинка.
// Еще через 3 секунды - третья.
// Еще через 3 секунды - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

const images = document.querySelectorAll('.image-to-show');
let count = 0;
const maxCount = images.length - 1;
let timeId = null;


function changeImg(arr, index) {
    const activeImg = document.querySelector('.active');
    if (activeImg){
        activeImg.classList.remove('active')
    }
    arr[index].classList.add('active');
    if (count < maxCount){
        count ++
    } else{
        count = 0
    }
};
timeId = setInterval(() => changeImg(images, count),500);



let buttonStop = document.querySelector('.end')
buttonStop.addEventListener("click", ()=> clearInterval(timeId))

let buttonStart = document.querySelector('.start');
buttonStart.addEventListener("click", () => timeId = setInterval(() => changeImg(images, count),500));

