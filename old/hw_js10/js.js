//Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// В файле index.html лежит разметка для двух полей ввода пароля.
// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
// Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.



        let input = document.querySelectorAll(".input");
        let inputFields = document.querySelectorAll("i");
        let button = document.querySelector(".btn");
        let text = document.createElement("p");
        text.style.display = "inline";
        text.textContent = "Нужно ввести одинаковые значения";

    inputFields.forEach((i, index) => i.addEventListener("click", event => {
        if(event.currentTarget.className === "fas fa-eye icon-password"){
          event.currentTarget.className = "fas fa-eye-slash icon-password";
          input[index].type = "text";
        }
        else {
          event.currentTarget.className = "fas fa-eye icon-password";
          input[index].type = "password";
        }
      }))
button.addEventListener("click", event => {
        event.preventDefault();
      if(input[0].value == input[1].value){
        alert("You are welcome!");
        text.remove();
      }
      else{
       input[1].after(text);
      }
      })