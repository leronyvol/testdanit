let tabs = document.querySelectorAll(".tabs-title");
let tabsText = document.querySelectorAll(".tabs-content-item");

tabs.forEach(function(elem){
    elem.addEventListener("click", function(){
        let current = elem;
        let tabId = current.getAttribute("data-title");
        let currentTab = document.querySelector(tabId);
        tabs.forEach(function(el){
            el.classList.remove("active");
        })
        tabsText.forEach(function(el){
            el.classList.remove("active");
        })
        current.classList.add("active")
        currentTab.classList.add("active")
    })
})