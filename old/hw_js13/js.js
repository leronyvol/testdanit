//Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы
/*let body = document.querySelector('body')
let changeColor = document.querySelector('.change-color')

changeColor.addEventListener("click", function () {
    if (body.style.backgroundColor === "white"){
        body.style.backgroundColor = "#94ffd7";
    } else{
        body.style.backgroundColor = "white"
    }

})*/

function setTheme(themeName) {
    localStorage.setItem("theme", themeName);
    document.documentElement.className = themeName;
}
function toggleTheme() {
    if (localStorage.getItem("theme") === "dark-theme") {
        setTheme("light-theme");
    } else {
        setTheme("dark-theme");
    }
}
if (localStorage.getItem("theme") === "dark-theme") {
    setTheme("dark-theme");
} else {
    setTheme("light-theme");
}
